const jwt = require("jsonwebtoken");

const secret = "CapstoneEcommerce";

module.exports.createAccessToken = (user) => {
  //Payload
  const data = {
    id: user._id,
    email: user.email,
    isAdmin: user.isAdmin,
  };

  return jwt.sign(data, secret, {});
};

//Token Verification

module.exports.verify = (req, res, next) => {
  //token retrieved from the request header
  let token = req.headers.authorization;

  //token received is not undefined
  if (typeof token !== "undefined") {
    console.log(token);

    token = token.slice(7, token.length);

    //verifying token using verify() method

    return jwt.verify(token, secret, (err, data) => {
      // if JWT is not valid
      if (err) {
        return res.send({ auth: "failed" });

        // if JWT is valid
      } else {
        //next action
        next();
      }
    });

    // if token is undefined
  } else {
    return res.send({ auth: "failed" });
  }
};

// Token Decryption

module.exports.decode = (token) => {
  // if token is defined
  if (typeof token !== "undefined") {
    token = token.slice(7, token.length);
    return jwt.verify(token, secret, (err, data) => {
      if (err) {
        return null;
      } else {
        // complete: true -> option that allows to return additional info from the JWT token
        // payload -> contains info provided in the "createAccessToken" (id, email, password, isAdmin)
        return jwt.decode(token, { complete: true }).payload;
      }
    });
  } else {
    return null;
  }
};
