//Setting up the Dependencies

const express = require("express");
const mongoose = require("mongoose");

const cors = require("cors");
const userRoutes = require("./Routes/userRoutes.js");
const productRoutes = require("./Routes/productRoutes.js");
const orderRoutes = require("./Routes/orderRoutes.js");

//Setting-up Server

const app = express();

//Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//Database connection
mongoose.connect(
  "mongodb+srv://admin:admin1234@b256legaspina.ssgp2cu.mongodb.net/B256_Ecommerce?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

// Routes
app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log(`Were connected to the cloud database`));

//Server Listening
app.listen(4000, () => console.log(`API is now online on port 4000`));
