//dependency
const mongoose = require("mongoose");

//Schema
const orderSchema = new mongoose.Schema({
  customers: [
    {
      userId: {
        type: String,
        required: [true, "User Id is required"],
      },
      orderedOn: {
        type: Date,
        default: new Date(),
      },
    },
  ],
  products: [
    {
      productId: {
        type: Array,
        items: {
          type: String,
        },
        required: [true, "Product Id is required"],
      },
      quantity: {
        type: Array,
        num: {
          type: Number,
        },
        required: [true, "Quantity is required"],
      },
    },
  ],
  totalAmount: {
    type: Number,
    required: [true, "Total Amount is required"],
  },
  purchasedOn: {
    type: Date,
    default: new Date(),
  },
});

//Export
module.exports = mongoose.model("Order", orderSchema);
