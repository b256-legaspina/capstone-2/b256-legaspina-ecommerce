// Dependency
const mongoose = require("mongoose");

// Schema
const userSchema = new mongoose.Schema ({

  firstName: {
    type: String,
    required: [true, "First Name is required"],
  },

  lastName: {
    type: String,
    required: [true, "Last Name is required"],
  },

  email: {
    type: String,
    required: [true, "Email Adress is required"],
  },

  number: {
    type: String,
    required: [true, "Number is required"],
  },

  password: {
    type: String,
    required: [true, "Password is required"],
  },

  isAdmin: {
    type: Boolean,
    default: false,
  },
});

//Export
module.exports = mongoose.model("User", userSchema);
