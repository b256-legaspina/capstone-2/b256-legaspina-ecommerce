const Order = require("../Models/order.js");

// Checkout Order

/*
    Business Logic:

    1. Create a new Order Object using the mongoose model and the information from the request body and id from the header
    2. Save the new Course to the database


*/

module.exports.checkOut = (order) => {
  let newOrder = new Order({
    customers: order.customers,
    products: order.products,
    totalAmount: order.totalAmount,
  });

  return newOrder.save().then((result, err) => {
    if (err) {
      return false;
    } else {
      return true;
    }
  });
};

module.exports.getAllOrders = () => {
  return Order.find({}).then((result) => {
    return result;
  });
};

module.exports.getOrderDetails = (reqParams) => {
  return Order.find(reqParams.customers).then((result) => {
    return result;
  });
};
