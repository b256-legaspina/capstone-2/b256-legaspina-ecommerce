const Product = require("../Models/product.js");

// Create a new Product

/**?
 * Business Logic
 * 1. Create a new product using the specific model and the information from the request body and the id from the header
 * 2. Save the new course to the database
 *
 * Model
 * name,
 * description,
 * price,
 * isActive,
 * createdOn,
 *
 */

module.exports.addProduct = (product) => {
  let newProduct = new Product({
    name: product.name,
    description: product.description,
    price: product.price,
  });

  return newProduct.save().then((result, err) => {
    // success
    if (err) {
      return false;

      // if failed
    } else {
      return true;
    }
  });
};

// Get All Products
/*
  Business Logic

returns all product in the database
*/

module.exports.getAllProducts = () => {
  return Product.find({}).then((result) => {
    return result;
  });
};

// Get Active Products

/*
  Business Logic

  returns the specific products with isActive = true property 

*/

module.exports.getActiveProducts = () => {
  return Product.find({ isActive: true }).then((result) => {
    return result;
  });
};

// Retrieving a specific product

/*
  Business Logic
  retrieve a product that matches the course ID inputted in the URL
*/
module.exports.getAProduct = (reqParams) => {
  return Product.findById(reqParams.productId).then((result) => {
    return result;
  });
};

// Edit a product

/*
  Business Logic
  1. Create a variable "editedProduct" which will contain the info retrieved from the request body
  2. Find and update the product (findByIdAndUpdate) using the product Id retrieved from the request params property and the variable "editProduct" containing the information from the request body 
*/
module.exports.editProduct = (product, paramsId) => {
  // to specify the field/properties that needs to be updated

  let editedProduct = {
    name: product.name,
    description: product.description,
    price: product.price,
  };

  //findByIdAndUpdate

  return Product.findByIdAndUpdate(paramsId.productId, editedProduct).then(
    (result, err) => {
      if (err) {
        return false;
      } else {
        return true;
      }
    }
  );
};

// Archive a Product

module.exports.archiveProduct = (reqParams) => {
  let updateActiveField = {
    isActive: false,
  };
  return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then(
    (product, error) => {
      if (error) {
        return false;
      } else {
        return true;
      }
    }
  );
};
