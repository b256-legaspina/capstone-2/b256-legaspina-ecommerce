// Dependencies
const User = require("../Models/user.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const { default: mongoose } = require("mongoose");

// Check if the email already exists
/*
	Business Logic: 
	1. Use mongoose "find" method to find duplicate emails
	2. Use the "then" method to send a response back to the frontend application based on the result of the "find" method
*/

module.exports.checkIfEmailExists = (requestBody) => {
  return User.find({ email: requestBody.email }).then((result) => {
    if (result.length > 0) {
      return true;
    } else {
      return false;
    }
  });
};

// User Registration
/*
	Business Logic:
	1. Create a new User object using the mongoose model and the information from the request body
	2. Make sure that the password is encrypted
	3. Save the new User to the database
*/

module.exports.registerUser = (requestBody) => {
  // Creates a variable "newUser" and instantiates a new "User" object using the mongoose model
  // Uses the information from the request body to provide all the necessary information

  let newUser = new User({
    firstName: requestBody.firstName,
    lastName: requestBody.lastName,
    email: requestBody.email,
    number: requestBody.number,
    password: bcrypt.hashSync(requestBody.password, 10),
  });

  return newUser.save().then((user, err) => {
    //User registration failed
    if (err) {
      return false;
      //User registration successful
    } else {
      return true;
    }
  });
};

// User authentication

/*
	Business Logic:
	1. Check the database if the user email exists
	2. Compare the password provided in the login form with the password stored in the database
	3. Generate/return a JSON web token if the user is successfully logged in and return false if not
*/

module.exports.authenticateUser = (requestBody) => {
  //searching for the collection which matches the search criteria
  return User.findOne({ email: requestBody.email }).then((result) => {
    // User not Found
    if (result == null) {
      return "error";

      // User exists
    } else {
      //create a variable then proceed to check the password
      const isPasswordCorrect = bcrypt.compareSync(
        requestBody.password,
        result.password
      );

      // if match
      if (isPasswordCorrect) {
        //generate token
        return { access: auth.createAccessToken(result) };

        // Passwords do not match
      } else {
        return false;
      }
    }
  });
};

// Controller for retrieving user details and changing the password to an empty string

module.exports.getUserDetails = (data) => {
  return User.findById(data.userId).then((result) => {
    result.password = "";
    return result;
  });
};
