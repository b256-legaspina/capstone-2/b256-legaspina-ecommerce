const express = require("express");
const router = express.Router();
const productController = require("../Controllers/productControllers.js");
const auth = require("../auth.js");

// Route for creating a product

router.post("/addProduct", auth.verify, (req, res) => {
  const data = {
    product: req.body,
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
  };

  if (data.isAdmin) {
    productController
      .addProduct(data.product)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send(false);
  }
});

// Route for retrieving all products

router.get("/all", (req, res) => {
  productController
    .getAllProducts()
    .then((resultFromController) => res.send(resultFromController));
});

// Route for retrieving all active products

router.get("/active", (req, res) => {
  productController
    .getActiveProducts()
    .then((resultFromController) => res.send(resultFromController));
});

// Route for retrieving a single product

router.get("/:productId", (req, res) => {
  productController
    .getAProduct(req.params)
    .then((resultFromController) => res.send(resultFromController));
});

// Route for updating a product

router.put("/edit/:productId", auth.verify, (req, res) => {
  const data = {
    product: req.body,
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
    params: req.params,
  };

  if (data.isAdmin) {
    productController
      .editProduct(data.product, data.params)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send(false);
  }
});

// route for archiving a product

router.put("/:productId/archive", auth.verify, (req, res) => {
  const data = {
    product: req.body,
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
    params: req.params,
  };

  if (data.isAdmin) {
    productController
      .archiveProduct(data.params)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send(false);
  }
});
module.exports = router;
