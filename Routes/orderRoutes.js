const express = require("express");
const router = express.Router();
// const productController = require("../Controllers/productControllers.js");
const userController = require("../Controllers/userControllers.js");
const orderController = require("../Controllers/orderControllers.js");
const auth = require("../auth.js");

// route for creating an order

router.post("/checkout", auth.verify, (req, res) => {
  const data = {
    order: req.body,
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
  };

  if (data.isAdmin) {
    res.send(false);
  } else {
    orderController
      .checkOut(data.order)
      .then((resultFromController) => res.send(resultFromController));
  }
});

// // route for retrieving all orders
// Route for retrieving all products

router.get("/all", (req, res) => {
  orderController
    .getAllOrders()
    .then((resultFromController) => res.send(resultFromController));
});

// Route for user Orders
router.get("/:userId/orderDetails", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  orderController
    .getOrderDetails(req.params)
    .then((resultFromController) => res.send(resultFromController));
});
// router.get;
module.exports = router;
