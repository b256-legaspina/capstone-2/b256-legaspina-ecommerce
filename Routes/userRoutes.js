// dependencies
const express = require("express");
const router = express.Router();
const userController = require("../Controllers/userControllers.js");
const auth = require("../auth.js");

// Check if email already exists

router.post("/checkEmail", (req, res) => {
  userController
    .checkIfEmailExists(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

// route for user registration
router.post("/register", (req, res) => {
  userController
    .registerUser(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

// route for user authentication

router.post("/login", (req, res) => {
  userController
    .authenticateUser(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

//route for user details

router.get("/userDetails", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  userController
    .getUserDetails({ userId: userData.id })
    .then((resultFromController) => res.send(resultFromController));
});

module.exports = router;
